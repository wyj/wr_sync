import numpy as np
import sys
import matplotlib.pyplot as plt

arg1 = int(sys.argv[1])
 
# specify the number of samples to save
# extract 10ms samples
# num_samples = 5760000
num_samples = 11520000
# num_samples = 3840000
#num_samples = 1920000

# open the input binary file for reading
with open('test.txt', 'rb') as f:
    # read the binary file contents as a byte string
    data = f.read()

# determine the number of complex numbers in the file
num_complex = len(data) // 8  # each complex number takes up 8 bytes
print("total num_complex: ", num_complex)

# make sure the number of requested samples is not greater than the number of complex numbers in the file
num_samples = min(num_samples, num_complex)
print(num_samples)
# unpack the complex numbers from the byte string
complex_array = np.array(np.frombuffer(data, np.complex64))
 
#print("before att: ",np.mean(np.abs(complex_array[(0+576*8):(576*9)])))
# get the first num_samples complex numbers
samples = complex_array[11520000+arg1:11520000+arg1+num_samples]
#samples = complex_array[5760000+arg1:5760000+arg1+num_samples] 
#samples = complex_array[3840000+arg1:3840000+arg1+num_samples]
#samples = complex_array[1920000+arg1:1920000+arg1+num_samples]
samples = np.array(samples,'complex64')
#print("after att: ",np.mean(np.abs(complex_array[5760000+576*8:5760000+576*9])))
 

#print(np.mean(np.abs(complex_array[1920000+1920*6:1920000+1920*7])))
#print(np.mean(np.abs(complex_array[1920000+1920*7:1920000+1920*8])))
#print(np.mean(np.abs(complex_array[1920000+1920*8:1920000+1920*9])))
print(np.mean(np.abs(complex_array[11520000+11520*6:11520000+11520*7])))
print(np.mean(np.abs(complex_array[11520000+11520*7:11520000+11520*8])))
print(np.mean(np.abs(complex_array[11520000+11520*8:11520000+11520*9])))
#print(np.mean(np.abs(complex_array[5760000+5760*6:5760000+5760*7])))
#print(np.mean(np.abs(complex_array[5760000+5760*7:5760000+5760*8])))
#print(np.mean(np.abs(complex_array[5760000+5760*8:5760000+5760*9])))
#print(np.mean(np.abs(complex_array[3840000+3840*6:3840000+3840*7])))
#print(np.mean(np.abs(complex_array[3840000+3840*7:3840000+3840*8])))
#print(np.mean(np.abs(complex_array[3840000+3840*8:3840000+3840*9])))
#print(len(samples))
#plt.plot(np.abs(samples[5760*7:5760*7+5760]))
#plt.plot(np.abs(samples[5760*8:5760*8+5760]))
#plt.show()
# write the selected complex numbers to a binary file
o = open('output.dat','wb')
samples.tofile(o)
o.close()
 
