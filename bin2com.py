#!/usr/bin/env python3
import numpy as np
import matplotlib.pyplot as plt

def read_bin_file(filename):
    with open(filename, "rb") as f:
        # Read the data as a binary string
        data = f.read()

    # Convert the binary string to a numpy array of complex numbers
    complex_array = np.array(np.frombuffer(data, np.complex64))

    return complex_array

def plot_complex_array(complex_array):
    # Plot the magnitude of the complex numbers
    plt.plot(np.abs(complex_array))
    plt.xlabel("Sample Index")
    plt.ylabel("Magnitude")
    plt.title("Magnitude of Complex Array")
    plt.show()

filename = "test.txt"
complex_array = read_bin_file(filename)
plot_complex_array(complex_array)