import numpy as np
import matplotlib.pyplot as plt

def read_binary_file(file_path):
    with open(file_path, 'rb') as f:
        data = f.read()

    complex_array = np.array(np.frombuffer(data, np.complex64))
    return complex_array

def plot_fft(samples, sample_rate):
    print("here")
    # Perform FFT
    fft = np.fft.fft(samples)

    # Plot the FFT
    fft_magnitude = np.abs(fft)
    fft_frequency = np.linspace(0.0, sample_rate, int(len(samples)))
    plt.plot(fft_frequency, fft_magnitude)
    plt.xlabel('Frequency (Hz)')
    plt.ylabel('Amplitude')
    plt.title('FFT Plot')
    plt.show()

if __name__ == '__main__':
    # File path and sample rate
    filename = 'test.txt'
    
    sample_rate = 300
    # Read binary file
    samples = read_binary_file(filename)
    print(samples)
    samples = samples[3000000:4000000]

    # Plot the FFT
    plot_fft(samples, sample_rate)
