#!/usr/bin/env python3
#
# Copyright 2017-2018 Ettus Research, a National Instruments Company
#
# SPDX-License-Identifier: GPL-3.0-or-later
#
"""
Generate and TX samples using a set of waveforms, and waveform characteristics
"""

import argparse
import numpy as np
import uhd
import time

def parse_args():
    """Parse the command line arguments"""
    parser = argparse.ArgumentParser()
    parser.add_argument("-a", "--args", default="", type=str)
    parser.add_argument("-f", "--freq", type=float, required=True)
    parser.add_argument("-r", "--rate", default=1e6, type=float)
    parser.add_argument("-d", "--duration", default=5.0, type=float)
    parser.add_argument("-c", "--channels", default=0, nargs="+", type=int)
    parser.add_argument("-g", "--gain", type=int, default=10)
    return parser.parse_args()


def main():
    """TX samples based on input arguments"""
    args = parse_args()
    usrp = uhd.usrp.MultiUSRP(args.args)

    # these lines are for frontend on dense nodes
    usrp.set_gpio_attr('FP0', "CTRL", 0)
    usrp.set_gpio_attr('FP0', "DDR", 0x10)
    usrp.set_gpio_attr('FP0', "OUT", 0x00)

    # Load the complex array from a file
    data = np.fromfile(open("tx.dat"), dtype=np.complex64)
    usrp.send_waveform(data, args.duration, args.freq, args.rate,
                       args.channels, args.gain)

if __name__ == "__main__":
    main()
