# WR_sync
Main branch has large data files, so try to clone with no-checkout first and then fetch master branch.

## Getting started


### Install library
```
sudo apt-get -y install python3-pip
pip3 install numpy
sudo apt-get install python3-matplotlib
```

### Debug WR time sync on dense points
1. Check if the code sets gpio correctly, since we have frontend on dense points
2. Modify the default UHD4.0.0 python library
```
git clone https://gitlab.flux.utah.edu/wyj/wr_sync.git
#git clone --no-checkout --filter=blob:none https://gitlab.flux.utah.edu/wyj/wr_sync.git 
#git checkout main
cd wr_sync
cp multi_usrp.py /usr/lib/python3/dist-packages/uhd/usrp
git checkout main -- iq_bin/meu3.txt
```
store the data to google drive via rclone
```
sudo -v ; curl https://rclone.org/install.sh | sudo bash
rclone config
rclone copy filename wr_sync:wr_sync
```
3. On RX nodes
```
cd wr_sync
python3 rx.py -o test.txt -f 3465e6 -d 2 -g 60

```
4. On TX nodes
```
cd wr_sync
python3 tx.py -f 3465e6 -d 1 -w square --wave-ampl 30 -g 85

```
### Debug WR time sync on rf-bench
1. Check if the code sets gpio correctly, since we don't have frontend on rf-bench, only Dense Deployment has frontend


2. Modify the default UHD4.0.0 python library
if there is no uhd installed, an easy way is to
```
apt-get install python3-uhd
apt-get install uhd-host
cd /usr/lib/uhd/utils
python3 uhd_images_downloader.py
uhd_find_devices
uhd_usrp_probe
```
replace the uhd python multi_usrp.py
```
git clone https://gitlab.flux.utah.edu/wyj/wr_sync.git
cp wr_sync/multi_usrp.py /usr/lib/python3/dist-packages/uhd/usrp
```

3. On RX node
```
cd wr_sync
python3 rx.py -o test.txt -f 3465e6 -d 2 -g 76

```
4. On TX node
```
cd wr_sync
python3 tx.py -f 3465e6 -d 1 -w square --wave-ampl 5 -g 89

```


### Debug WR time sync on rf-bench with UHD4.4.0
1. Uninstall python3-uhd if installed
check with: (where you can see all the pkg related to uhd)
```
dpkg -l | grep uhd
```
Remove the pkg:
```
sudo apt remove python3-uhd
```
2. Install UHD4.4.0 from source, help on [how to build UHD from source](https://files.ettus.com/manual/page_build_guide.html). Make sure (a) when cmake the file enable **-DENABLE_PYTHON_API=ON** (b) make sure **uhd_usrp_probe** works properly.
3. Build python3-uhd (in case the new python3-uhd didn't install)
```
cd uhd/host/build/python/
python3 setup.py build
python3 setup.py install
```
Now we should be able to set the timespec for streamer y directly give the timespce to the send and recv function.

4. Replace the multi_usrp under /usr/local/lib/python3.6/dist-packages/uhd-4.4.0-py3.6.egg/uhd/usrp/
```
cd wr_sync/uhd4.4.0
sudo cp multi_usrp.py /usr/local/lib/python3.6/dist-packages/uhd-4.4.0-py3.6.egg/uhd/usrp/
```
UHD4.4.0 stops me somewhere at:
```
streamer = super(MultiUSRP, self).get_tx_stream(st_args)
```
The error is: **RuntimeError: RuntimeError: tx xport timed out wating for a strs packet during initialization**
The error comes from **get_recv_buff(int32_t timeout_ms)**, where a timeout_ms argument is set. However, I have to get a new streamer for each burst.
Have to go back using UHD4.0.0


## ToDo list
- [ ] Check TX sync  pairwise comparisonv
- [ ] Check TX-RX sync

7.68MHz => 130 ns

| time_sync | rf-bench |   ustar-> ebc/moran |
| ------ | ------ |------ |
| tx sync          |               |   < 100 ns |
|   tx-rx sync     |     40~45 us  |    11 us   |



ustar <- ebc/moran TX sync (100 feet, 101s)

|sample rate	|1e6	 |1e7	|2e7	|3e7 |
| ------ | ------ | ------ | ------ | ------ |
|diff(samps)	|0	|0	|3	|5 |
|diff(time)	| < 1us	| < 100ns | 150ns | 170ns |


ebc <- ustar/moran TX sync (300 feet, 305ns)

|sample rate	|1e6	 |1e7	|2e7	|3e7 |
| ------ | ------ | ------ | ------ | ------ |
|moran(samps)	|8	|59	|189	|202|
|ustar(samps)	|7~8	|58	|186	|196|
|diff(samps)	| 0~1	|0~1	|3	|6 |
|diff(time)	|< 1us	|< 100ns | 150ns |200ns|


moran <- ustar/ebc TX sync (400 feet, 406ns)
|sample rate	|1e6	 |1e7	|2e7	|3e7 |
| ------ | ------ | ------ | ------ | ------ |
|ebc(samps)	    |10	    |60	         |192	|200|
|ustar(samps)	|9~10	|56~59	     |184	|188|
|diff(samps)	| 0~1	|1~4	     |8 	|12 |
|diff(time)	    |< 1us	|< 100~400ns | 400ns |400ns|


## SRSRAN pdsch_enodeb and pdsch_ue

```
cp ~/wr_sync/enb/rf_uhd_generic.h ~/srsRAN/lib/src/phy/rf/
cp ~/wr_sync/enb/rf_uhd_safe.h ~/srsRAN/lib/src/phy/rf/
```

## Check UHD packet time line within one burst
Replace the multi_usrp.py with the one in pkt_TLV. 
Waveforms are generated in multi_usrp.py in a high low high low pattern whose durationn based on the buffer sample size.
