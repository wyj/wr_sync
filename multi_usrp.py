#
# Copyright 2017-2018 Ettus Research, a National Instruments Company
#
# SPDX-License-Identifier: GPL-3.0-or-later
#
""" @package usrp
Python UHD module containing the MultiUSRP and other objects
"""

import numpy as np
from .. import libpyuhd as lib
import time

class MultiUSRP(lib.usrp.multi_usrp):
    """
    MultiUSRP object for controlling devices
    """
    def __init__(self, args=""):
        """MultiUSRP constructor"""
        super(MultiUSRP, self).__init__(args)

    def recv_num_samps(self, num_samps, freq, rate=1e6, channels=(0,), gain=10):

        """
        RX a finite number of samples from the USRP
        :param num_samps: number of samples to RX
        :param freq: RX frequency (Hz)
        :param rate: RX sample rate (Hz)
        :param channels: list of channels to RX on
        :param gain: RX gain (dB)
        :return: numpy array of complex floating-point samples (fc32)
        """
        # Set UHD clock to external and rx_rate
        super(MultiUSRP, self).set_clock_source("external")
        super(MultiUSRP, self).set_time_source("external")
        print(super(MultiUSRP, self).get_sync_source(0))
        print(super(MultiUSRP, self).get_mboard_sensor_names(0))
        ref_locked = 0
        for i in range (30):
            ref_locked = super(MultiUSRP, self).get_mboard_sensor("ref_locked",0)
            #gps_locked = usrp.get_mboard_sensor("gps_locked",0)
            if not ref_locked: #and not gps_locked:
                time.sleep(1)
        if ref_locked:
            print("ref locked")
        else:
            print("ref failed")

        result = np.empty((len(channels), num_samps), dtype=np.complex64)

        for chan in channels:
            super(MultiUSRP, self).set_rx_rate(rate, chan)
            super(MultiUSRP, self).set_rx_freq(lib.types.tune_request(freq), chan)
            super(MultiUSRP, self).set_rx_gain(gain, chan)


        # Reset UHD timer to 0.0 using next next PPS    
        time_at_last_pps = super(MultiUSRP, self).get_time_last_pps().get_real_secs()
        while time_at_last_pps == super(MultiUSRP, self).get_time_last_pps().get_real_secs():
            time.sleep(0.1) # keep waiting till it happens- if this while loop never finishes then the PPS signal isn't there
        super(MultiUSRP, self).set_time_next_pps(lib.types.time_spec(0.0))
        print(super(MultiUSRP, self).get_time_now().get_real_secs())
        time.sleep(1)
        print(super(MultiUSRP, self).get_time_now().get_real_secs())
            
        # Prepare recv streamer
        st_args = lib.usrp.stream_args("fc32", "sc16")
        st_args.channels = channels
        metadata = lib.types.rx_metadata()
        streamer = super(MultiUSRP, self).get_rx_stream(st_args)
        buffer_samps = streamer.get_max_num_samps()
        recv_buffer = np.zeros(
            (len(channels), buffer_samps), dtype=np.complex64)

        recv_samps = 0
        stream_cmd = lib.types.stream_cmd(lib.types.stream_mode.start_cont)
        #stream_cmd.stream_now = True

        # Set timespec for recving samples 
        stream_cmd.stream_now = False
        stream_cmd.time_spec = lib.types.time_spec(3.0)
        streamer.issue_stream_cmd(stream_cmd)
        #print(super(MultiUSRP, self).get_time_now().get_real_secs())


        samps = np.array([], dtype=np.complex64)
        while recv_samps < num_samps:
            samps = streamer.recv(recv_buffer, metadata,timeout=3.1)

            if metadata.error_code != lib.types.rx_metadata_error_code.none:
                print(metadata.strerror())
            if samps:
                real_samps = min(num_samps - recv_samps, samps)
                result[:, recv_samps:recv_samps + real_samps] = recv_buffer[:, 0:real_samps]
                recv_samps += real_samps

        #print(super(MultiUSRP, self).get_time_now().get_real_secs())
        stream_cmd = lib.types.stream_cmd(lib.types.stream_mode.stop_cont)
        streamer.issue_stream_cmd(stream_cmd)
        while samps:
            samps = streamer.recv(recv_buffer, metadata)
        # Help the garbage collection
        streamer = None
        return result

    def send_waveform(self,
                      waveform_proto,
                      duration,
                      freq,
                      rate=1e6,
                      channels=(0,),
                      gain=10):
        """
        TX a finite number of samples from the USRP
        :param waveform_proto: numpy array of samples to TX
        :param duration: time in seconds to transmit at the supplied rate
        :param freq: TX frequency (Hz)
        :param rate: TX sample rate (Hz)
        :param channels: list of channels to TX on
        :param gain: TX gain (dB)
        :return: the number of transmitted samples
        """


        # Set external clock and tx_rate
        super(MultiUSRP, self).set_clock_source("external")
        super(MultiUSRP, self).set_time_source("external")
        print(super(MultiUSRP, self).get_sync_source(0))
        print(super(MultiUSRP, self).get_mboard_sensor_names(0))
        ref_locked = 0
        for i in range (30):
            ref_locked = super(MultiUSRP, self).get_mboard_sensor("ref_locked",0)
            #gps_locked = usrp.get_mboard_sensor("gps_locked",0)
            if not ref_locked: #and not gps_locked:
                time.sleep(1)

        if ref_locked:
            print("ref locked")
        else:
            print("ref failed")

        for chan in channels:
            super(MultiUSRP, self).set_tx_rate(rate, chan)
            super(MultiUSRP, self).set_tx_freq(lib.types.tune_request(freq), chan)
            super(MultiUSRP, self).set_tx_gain(gain, chan)

        # Prepare tx streamer
        st_args = lib.usrp.stream_args("fc32", "sc16")
        st_args.channels = channels

        # Send next burst at 3.0, 2.0 on two different nodes to test WR tx sync
        x = lib.types.time_spec(3.0)

        # Reset UHD timer to 0.0
        time_at_last_pps = super(MultiUSRP, self).get_time_last_pps().get_real_secs()
        while time_at_last_pps == super(MultiUSRP, self).get_time_last_pps().get_real_secs():
            time.sleep(0.1) # keep waiting till it happens- if this while loop never finishes then the PPS signal isn't there
        super(MultiUSRP, self).set_time_next_pps(lib.types.time_spec(0.0))
        #print(super(MultiUSRP, self).get_time_now().get_real_secs())
        time.sleep(1)
        #print(super(MultiUSRP, self).get_time_now().get_real_secs())
        
        # Send 5 bursts in total
        for i in range(5):  
            streamer = super(MultiUSRP, self).get_tx_stream(st_args)
            buffer_samps = streamer.get_max_num_samps()
            proto_len = waveform_proto.shape[-1]

            if proto_len < buffer_samps:
                waveform_proto = np.tile(waveform_proto,
                                     (1, int(np.ceil(float(buffer_samps)/proto_len))))
                proto_len = waveform_proto.shape[-1]

            metadata = lib.types.tx_metadata()
            metadata.has_time_spec=True
            metadata.time_spec = x
            send_samps = 0
            max_samps = int(np.floor(duration * rate))

            if len(waveform_proto.shape) == 1:
                waveform_proto = waveform_proto.reshape(1, waveform_proto.size)
            if waveform_proto.shape[0] < len(channels):
                waveform_proto = np.tile(waveform_proto[0], (len(channels), 1))

            while send_samps < max_samps:
                real_samps = min(proto_len, max_samps-send_samps)
                if real_samps < proto_len:
                    samples = streamer.send(waveform_proto[:, :real_samps], metadata)
                else:
                    samples = streamer.send(waveform_proto, metadata)
                send_samps += samples

            # Clean the streamer    
            metadata.end_of_burst = True
            streamer.send(np.zeros((len(channels), 1), dtype=np.complex64), metadata)
            # Send a burst every 3 seconds
            x = x+lib.types.time_spec(3.0)
            # Help the garbage collection
            streamer = None
        return send_samps
