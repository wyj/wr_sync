import uhd
import numpy as np
import time

usrp = uhd.usrp.MultiUSRP()

num_samps = int(3072000) # number of samples received
center_freq = 2506.95e6 # Hz
sample_rate = 30.72e6 # Hz
gain = 50 # dB

usrp.set_rx_rate(sample_rate, 0)
usrp.set_rx_freq(uhd.libpyuhd.types.tune_request(center_freq), 0)
usrp.set_rx_gain(gain, 0)

usrp.set_clock_source("gpsdo")
usrp.set_time_source("gpsdo")

gps_locked = 0
ref_locked = 0
while  not ref_locked or not gps_locked:
    ref_locked = usrp.get_mboard_sensor("ref_locked", 0).to_bool();
    gps_locked = usrp.get_mboard_sensor("gps_locked", 0).to_bool();
    time.sleep(0.1)
 
print("ref and gps locked!")

# Set up the stream and receive buffer
st_args = uhd.usrp.StreamArgs("fc32", "sc16")
st_args.channels = [0]
metadata = uhd.types.RXMetadata()
streamer = usrp.get_rx_stream(st_args)
recv_buffer = np.zeros((1, 1000), dtype=np.complex64)

time_at_last_pps = usrp.get_time_last_pps().get_real_secs()
while time_at_last_pps == usrp.get_time_last_pps().get_real_secs():
    time.sleep(0.1) # keep waiting till it happens- if this while loop never finishes then the PPS signal isn't there
usrp.set_time_next_pps(uhd.libpyuhd.types.time_spec(0.0))
print(usrp.get_time_now().get_real_secs())
time.sleep(1)

# Schedule Rx of num_samps samples exactly 3 seconds from last PPS
stream_cmd = uhd.types.StreamCMD(uhd.types.StreamMode.num_done)
stream_cmd.num_samps = num_samps
stream_cmd.stream_now = False
stream_cmd.time_spec = uhd.libpyuhd.types.time_spec(3.0) # set start time (try tweaking this)
streamer.issue_stream_cmd(stream_cmd)
print(usrp.get_time_now().get_real_secs())
# Receive Samples.  recv() will return zeros, then our samples, then more zeros, letting us know it's done
waiting_to_start = True # keep track of where we are in the cycle (see above comment)
nsamps = 0
i = 0
samples = np.zeros(num_samps, dtype=np.complex64)
while nsamps != 0 or waiting_to_start:
    nsamps = streamer.recv(recv_buffer, metadata)
    if nsamps and waiting_to_start:
        waiting_to_start = False
    elif nsamps:
        samples[i:i+nsamps] = recv_buffer[0][0:nsamps]
    i += nsamps
    
out_file = "../syncscan/tools/test.cfile"
with open(out_file, 'wb') as out_file:
    samples.tofile(out_file)
print("end")
 
print(len(samples))
print(samples[0:10])
